<?php

class PersonaHeredera
{
    //propiedades
    public $nombre, $edad;
    public $apellido1, $apellido2;

    public function setNombre($nombre)
    {
        $this->nombre = strtolower($nombre);
    }

    public function getNombre()
    {
        return ucwords($this->nombre);
    }

    public function setApellidos($apellido1, $apellido2)
    {
        $this->apellido1 = $apellido1;
        $this->apellido2 = $apellido2;
    }

    public function getApellido()
    {
        return $this->apellido1 . " " . $this->apellido2;
    }
}

class Peruano extends PersonaHeredera
{
    public $departamento, $ciudad;

    public function setApellidos($apellido1, $apellido2)
    {
        //En vez de sobreescribir, extendemos
        parent::setApellidos($apellido1, $apellido2);

        echo "Los apellidos se asignaron con éxito";
    }
}


//------------------------------ Va en el index.php -----------------------------------------

class Chileno extends PersonaHeredera
{
    //Sobreescribir método del padre
    public function setApellidos($apellido1, $apellido2)
    {
        $this->apellido1 = $apellido2;
        $this->apellido2 = $apellido1;
    }
}

$peruano = new Peruano;
// $peruano->setNombre("Anibal Lecter");

$chileno = new Chileno;
$chileno->setApellidos("Arana", "Florez");

//var_dump($peruano);
//echo "<br>";
//var_dump($chileno);
echo $chileno->getApellido();
echo "<br>";
$peruano->setApellidos("Arana", "Florez");
echo "<br>";
echo $peruano->getApellido();
echo "<br>";
var_dump($peruano);

?>