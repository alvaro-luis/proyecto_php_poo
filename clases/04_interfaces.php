<?php

interface iTemplate
{
    public function setVariable();
    public function getHtml();
}

//Clase con interfaz
class Template implements iTemplate
{
    public function setVariable()
    {
        
    }
	public function getHtml()
    {

	}
}

interface a
{
    public function prueba1();
}

interface b extends a
{
    public function prueba2();
}

interface c extends a,b
{
    public function prueba3();
}

//Al implementar interfaz b, también se debe implementar la interfaz a
class MyClass implements b
{
	public function prueba1()
    {

	}

	public function prueba2() 
    {

	}
}

class MyClass2 implements c
{
	public function prueba1()
    {

	}

	public function prueba2()
    {

	}
	
	public function prueba3()
    {

	}
}

?>