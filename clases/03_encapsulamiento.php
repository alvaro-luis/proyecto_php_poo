<?php

class MyClass
{
    public $public = "Public";
    protected $protected = "Protected";
    private $private = "Private";

    //cuando no hay un "public" o "prvate
    function printHello()
    {
        echo $this->public . "<br> ";
        echo $this->protected . "<br> ";
        echo $this->private . "<br> ";
    }
}

$objeto = new MyClass;

echo "Acceder a propiedades de la clase que sean accesibles:<br> ";
echo $objeto->public;
echo "<br> ";
// echo $objeto->protected;
// echo "<br> ";
// echo $objeto->private;
echo "<br> ";

echo "Acceder a propiedades de la clase sea cual sea su nivel de acceso a través de un método:<br> ";
//echo "<br> ";
$objeto->printHello();
echo "<br> ";
echo "<br> ";

class MyClass2 extends MyClass
{
    public $public = "Public 2";
    protected $protected = "Protected 2";

    //Podemos acceder a variables protegidas desde la clase que hereda
    function printHello()
    {
        echo $this->public . "<br> ";
        echo $this->protected . "<br> ";
        echo $this->private . "<br> ";
    }
}

echo "Acceder a propiedades de la clase sea cual sea su nivel de acceso a través de un método Desde MyClass2:<br> ";
$obj2 = new MyClass2;
$obj2->printHello();

?>