<?php

abstract class ClaseAbstracta
{
    abstract protected function getValor();
    abstract public function valorPrefijo($prefijo);

    public function imprimir()
    {
        echo $this->getValor();
    }
}

//$objeto = new ClaseAbstracta esto no se puede hacer con las clases abstractas

//Las clases abstractas solo pueden ser extendidas desde otras clases
class ClaseConcreta extends ClaseAbstracta
{
    protected function getValor()
    {
        return "Clase concreta";
    }

    public function valorPrefijo($prefijo, $separador = '.')
    {
        //Lógica del método
        return $prefijo . "Clase concreta";
    }
}

$clase1 = new ClaseConcreta;

$clase1->imprimir();
echo "<br>";
echo $clase1->valorPrefijo("Prefijo de ")

?>