<?php

require_once('persona.php');

//objeto generado
//$persona1 = new Persona;
$persona1 = new Persona("FulanO SuTAno","Fulanez Hernández", 25);

echo "Objeto instanciado luego de inicializar: <br>";
var_dump($persona1);

echo "<br>";
echo "<br>";

//$persona1 -> nombre = "Fulano Sutano";
//$persona1->setNombre("FulanO SuTAno");

/*Utlizamos un método para procesar la información introducida por el usuario de forma correcta
  según nuestras necesidades */

//$persona1 -> apellido = "Fulanez Hernández";
//$persona1 -> edad = 25;
$persona1 -> nombre = "Fulano";

echo "Objeto instanciado luego de asignar valores a propiedades: <br>";
var_dump($persona1);

//instancia de la clase persona
//$persona2 = new Persona;
$persona2 = new Persona("Iris Elizabeth","Godoy", 28);

//$persona2 -> nombre = "Iris Elizabeth";
//$persona2->setNombre("Iris Elizabeth");
//$persona2 -> apellido = "Godoy";
//$persona2 -> edad = 28;

echo "<br>";
echo "<br>";

//echo "El nombre de la persona 1 es: " .$persona1 ->nombre."<br>";
echo "El nombre de la persona 1 es: " .$persona1 ->getNombre()."<br>";
echo "El nombre de la persona 2 es: " .$persona2 ->getNombre()."<br>";

?>